let content;
let key;
let container = document.querySelector('.list');
function getFilm() {
  return fetch('https://swapi.dev/api/films/')
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    content = data.results;
    for(key in content) {
      container.innerHTML += `<li>
      <h2> Episode № ${content[key].episode_id} </h2>
      <h3> ${content[key].title}</h3>
      <p class="char-${key}"> </p>
      <p> ${content[key].opening_crawl}</p>      
      </li>`
    }
  })
}

async function getChar() {
  await getFilm();
  content.forEach((el, i) => {
    el.characters.forEach((e) => {
      fetch(e)
      .then((response) => {
      return response.json();
    })
    .then((data) => {
      document.getElementsByClassName(`char-${i}`)[0].append(data.name)
  })
  })
  })
}

getChar()

