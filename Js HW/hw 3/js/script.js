class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get fullConstruct () {
    return `${this.name} ${this.age} my salary is ${this.salary}`
  }
  set fullConstruct(value) {
    const split = value.split(' ');
    this.name = split[0];
    this.age = split[1];
  }
}

class Programmer extends Employee {
  constructor (name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  } 
  get salaryX() {
    return `$${this.salary * 3}`;
  }
} 

const person = new Employee('Im', 23, '$10000');
console.log(person);
console.log(person.fullConstruct);
person.fullConstruct = 'oleg vorobushek'
console.log(person.name);
console.log(person.age);

const person2 = new Programmer('whoAre', 24, 15000, 'English, Russian, Spanish')
console.log(person2);
console.log(person2.salaryX);

const person3 = new Programmer('Rocket', 25, 30000, "English, Espanol, Japanese, Russian");
console.log(person3);
person3.fullConstruct = 'Yarik Day-33-Vedra';
console.log(person3.name);
console.log(person3.age);
