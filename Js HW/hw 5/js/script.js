const btnIp = document.createElement('button');
btnIp.innerHTML = 'Вычислить по IP';
document.body.append(btnIp);
const requestURL = url => fetch(url).then(response => response.json())

let ipConst;
btnIp.addEventListener("click", getIp)

const BASE_URL = 'https://api.ipify.org/?format=json';
const second_url = 'http://ip-api.com/json/';

async function getIp() {
const myIp = await requestURL(BASE_URL);
    const {ip} = myIp;
    ipConst = ip;
    getGeo();
}

async function getGeo() {
  await getIp;
    const geo = await requestURL(`${second_url}${ipConst}`);
    document.body.insertAdjacentHTML('beforeend', `<br> <span>${geo.timezone}</span> <br> <span>${geo.country}</span> 
    ${geo.region}<br> <span>${geo.city} </span><br>  <span>${geo.zip}</span>`)
}




// "use strict";

// const btnIp = document.querySelector("#btn-ip");
// let ipResponseUser;
// btnIp.addEventListener("click", getIp);

// async function getIp(params) {
//   await fetch("https://api.ipify.org/?format=json")
//     .then((response) => {
//       return response.json();
//     })
//     .then((data) => {
//       const { ip } = data;
//       ipResponseUser = ip;
//     });
//   getHackerContacts();
// }

// async function getHackerContacts() {
//   await getIp;
//   await fetch(`http://ip-api.com/json/${ipResponseUser}`, {
//     mode: "cors",
//   })
//     .then((response) => {
//       return response.json();
//     })
//     .then((data) => {
//       console.log(data);
//     });
// }