const books = [
  { 
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70 
  }, 
  {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
  }, 
  { 
    name: "Тысячекратная мысль",
    price: 70
  }, 
  { 
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  }, 
  {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
  },
  {
   author: "Дарья Донцова",
   name: "Дед Снегур и Морозочка",
  }
];

const root = document.querySelector('#root');
// const ulList = document.createElement('ul');
// root.append(ulList);

const arr = books.map(function (obj) {
  const {name, author, price} = obj;
  try {
  if (name && author && price) {
  return `<li> Название: ${name}. Автор: ${author}. Цена: ${price}</li>`
} else if (!name) {
  throw new Error('name is not defined');
} else if (!author) {
  throw new Error('author is not defined');
} else if (!price) {
  throw new Error('price is not defined');
}
  } catch (e) {
    console.error(e);
  }
})

root.insertAdjacentHTML('beforeend', `<ul>${arr.join('')}  </ul>`)

console.log(arr);

// function createLi() {
//   for (let i = 0; i < arr.length; i++) {
//     try {
//     const newLi = document.createElement('li');
//     if (arr[i] !== undefined ) {
//     newLi.innerHTML = arr[i];
//     ulList.append(newLi)
//     } else if (arr[i] == undefined) {
//       throw new Error('not full info');
//     }
//   } catch (err){
//     console.error(err);
//   }
//   }  
// }
// // createLi()
