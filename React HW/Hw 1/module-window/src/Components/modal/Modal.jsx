import React, {Component} from 'react'


class Modal extends Component {
  render() {
    let {header, closeBtn, text, actions, closeModal} = this.props
      if (closeBtn) {
        closeBtn = 'X'
      }

    return (
        <div onClick={closeModal} className="modal show">
          <div className="modal-dialog">
            <div className='modal-content'>
            <div className='modal-header'>{header}
                <span onClick={closeModal} className='close-Btn'>
                  {closeBtn}
                </span>
            </div>
              <div className='modal-body'>{text}
              </div>
            <div className='modal-body btns'>
              {actions}
              </div>
            </div>
          </div>
      </div>
    )}
}
export default Modal


