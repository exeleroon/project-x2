import React, {Component} from 'react'

class Button extends Component {
    render() {
        const {bgColor, text, toggleModal, className} = this.props
        return (
            <button onClick={toggleModal} style={{background:bgColor}} className={className}>{text}</button>
        )
    }
}

export default Button 