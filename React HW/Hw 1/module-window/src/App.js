import React from 'react'
import './App.css';
import { Component } from 'react';
import Modal from './Components/modal/Modal'
import Button from './Components/buttons/BtnComponent';

class App extends React.Component {
  state = {
    isOpenFirstModal: false,
    isOpenSecondModal: false
  }

  openFirstModal = () => {
    this.setState({isOpenFirstModal: !this.state.isOpenFirstModal})
  }

  openSecondModal = () => {
    this.setState({isOpenSecondModal: !this.state.isOpenSecondModal})
  }

  closeModalWindow = (e) => {
    if (this.state.isOpenFirstModal && e.target.classList.contains('modal')) {
      this.setState({isOpenFirstModal: !this.state.isOpenFirstModal})
    }
    if (this.state.isOpenSecondModal && e.target.classList.contains('modal')) {
      this.setState({isOpenSecondModal: !this.state.isOpenSecondModal})
    }
  }





  render() {
   return (
      <>
        {this.state.isOpenFirstModal && (
            <Modal header={'Do you want to delete this file?'} closeBtn={true} closeModal={this.closeModalWindow}
            text={"Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"} 
              actions={(
              <>
                <Button bgColor={'rgba(0, 0, 0, 0.219)'} className={'modalBtn'} text={'OK'} />
                <Button bgColor={'rgba(0, 0, 0, 0.219)'} className={'modalBtn'} text={'Cancel'} />
                </>
              )}
            />
          )} 
        
         {this.state.isOpenSecondModal && (
          <Modal header={'Do you want to fun?'} closeBtn={true} closeModal={this.closeModalWindow}
                text={"Once you delete this file file file ERRORRR"} 
              actions={(
                <>
                  <Button bgColor={'orange'} className={'modalBtn'} text={'Submit'} />
                  <Button bgColor={'blue'} className={'modalBtn'} text={'Return'} />
                  </>
            )}
          />
        )}

        <div className='btn-container'>
          <Button toggleModal={this.openFirstModal}  bgColor={'green'} text={'Open first modal'} className={'mainBtn'} />
          <Button toggleModal={this.openSecondModal}  bgColor={'purple'} text={'Open second modal'} className={'mainBtn'} />
        </div>
      </>
    ) 
  }
}


export default App;
