import "./App.css";
import React, { useEffect, useState } from "react";
import ProductInCart from "./ProductInCart";

const CartGoods = ({ cartProducts, delCartItem, allGoods }) => {
  const [isLoading, setisLoading] = useState(true);
  const [productsInCart, setproductsInCart] = useState(cartProducts);
  // Проверяем пришли ли продукты с сервера
  useEffect(() => {
    if (allGoods.length > 0) {
      setisLoading(false);
    }
  }, [allGoods]);

  useEffect(() => {
    setproductsInCart(cartProducts);
  }, [cartProducts]);

  // Формируем массив обектов (товары в корзине)
  const cartProductsItem = [];
  // Запускаем этот код только после того как статус isLoading поменялся на false (т.е. загрузка завершилась)
  if (!isLoading) {
    productsInCart.forEach((id) => {
      const targetProduct = allGoods.find((item) => item.id === id);
      cartProductsItem.push(targetProduct);
    });
  }
  return (
    <div className="cards">
      {isLoading ? (
        <p>Products is loading ...</p>
      ) : (
        cartProductsItem.map((product) => (
          <ProductInCart
            delCartItem={delCartItem}
            product={product}
            key={product.id}
          />
        ))
      )}
    </div>
  );
};

export default CartGoods;
