import "./App.css";
import React from "react";
import Cards from "./cards";

const Goods = ({ showGoods, onModal, toggleFav, favGoods }) => {
  const cards = showGoods.map((product) => {
    return (
      <Cards
        favGoods={favGoods}
        toggleFav={toggleFav}
        onModal={onModal}
        product={product}
        key={product.id}
      />
    );
  });

  return (
    <>
      <div className="cards">{cards}</div>
    </>
  );
};

export default Goods;
