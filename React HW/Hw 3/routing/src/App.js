import "./App.css";
import React, { useEffect, useState } from "react";
import {  Switch, useHistory, Route} from "react-router-dom";
import cart from "./cart.svg";
import star from "./unstar.svg";
import homeIcon from "./home.svg";
import getGoods from "./Api/goodsItems";
import Goods from "./Goods";
import Favorite from "./Favorite";
import CartGoods from "./Cart";
import Modal from "./Components/modal/Modal";
import Button from "./Components/buttons/BtnComponent";

const App = () => {
  const [goods, setGoods] = useState([]);
  const [goodsCart, setGoodsCart] = useState([]);
  const [favGoods, setFavGoods] = useState([]);
  const [isShowModal, setIsShowModal] = useState(false);
  const [currentCartId, setCurrentCartId] = useState("");
  const [isShowModalDelete, setIsShowModalDelete] = useState(false);

  const [counterFav, setCounterFav] = useState(0);
  const [counterCart, setCounterCart] = useState(0);

  useEffect(() => {
    getGoods().then((goods) => {
      setGoods(goods);
    });

    if (localStorage.getItem("cartItems")) {
      const getItemsLocStor = localStorage.getItem("cartItems").split(",");
      setGoodsCart(getItemsLocStor);
      setCounterCart(getItemsLocStor.length);
    }
    if (localStorage.getItem("favItems")) {
      const getItemsLStor = localStorage.getItem("favItems").split(",");
      setFavGoods(getItemsLStor);
      setCounterFav(getItemsLStor.length);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("cartItems", goodsCart);
  }, [goodsCart]);

  useEffect(() => {
    localStorage.setItem("favItems", favGoods);
  }, [favGoods]);

  const handleModalOpen = (id) => {
    setIsShowModal(true);
    setCurrentCartId(id);
  };

  const deleteModalOpen = (id) => {
    setIsShowModalDelete(true);
    setCurrentCartId(id);
  };

  const handleModalClose = () => {
    setIsShowModal(false);
    setIsShowModalDelete(false);
  };

  const addToCart = () => {
    if (goodsCart.includes(currentCartId)) {
      alert("Product already in cart");
      handleModalClose();
    } else {
      setGoodsCart([currentCartId, ...goodsCart]);
      handleModalClose();
      setCounterCart(counterCart + 1);
    }
  };

  const addToFavorite = (id) => {
    if (favGoods.includes(id)) {
      const newfavGoods = favGoods.filter((favId) => favId !== id);
      setFavGoods(newfavGoods);
      setCounterFav(counterFav - 1);
    } else {
      setFavGoods([id, ...favGoods]);
      setCounterFav(counterFav + 1);
    }
  };

  const removeFromCart = () => {
    const newGoodsCarts = goodsCart.filter((item) => item !== currentCartId);
    setGoodsCart(newGoodsCarts);
    setIsShowModalDelete(false);
    setCounterCart(newGoodsCarts.length);
  };

  const removeFromFavorite = (id) => {
    const newGoodsFav = favGoods.filter((item) => item !== id);
    setFavGoods(newGoodsFav);
    setCounterFav(newGoodsFav.length);
  };

  const cartActions = (
    <div>
      <Button
        toggleBtn={addToCart}
        bgColor={"rgba(0, 0, 0, 0.219)"}
        className={"modalBtn close"}
        text={"Add to cart"}
      />
      <Button
        toggleBtn={handleModalClose}
        bgColor={"rgba(0, 0, 0, 0.219)"}
        className={"modalBtn close"}
        text={"Cancel"}
      />
    </div>
  );

  const deleteCartActions = (
    <div>
      <Button
        toggleBtn={removeFromCart}
        bgColor={"rgba(0, 0, 0, 0.219)"}
        className={"modalBtn close"}
        text={"Delete"}
      />
      <Button
        toggleBtn={handleModalClose}
        bgColor={"rgba(0, 0, 0, 0.219)"}
        className={"modalBtn close"}
        text={"Cancel"}
      />
    </div>
  );

  const history = useHistory();

  const handleClick = (name) => {
    history.push(`${name}`);
  };

  const locationCart = "cart";
  const locationFavorite = "favorite";
  const home = "/";

  return (
    <>
      <button onClick={() => handleClick(home)} className="homeIcon">
        <img src={homeIcon}  alt="home icon"></img>
      </button>
      <button onClick={() => handleClick(locationCart)} className="btn-cart">
        {" "}
        <span className="cart-c">{counterCart}</span>
        <img className="cart" src={cart} alt="cart"></img>
      </button>
      <button
        onClick={() => handleClick(locationFavorite)}
        className="btn-favorite"
      >
        {" "}
        <span className="fav-c">{counterFav}</span>
        <img className="favorite" src={star} alt="starfav"></img>
      </button>

      <Switch>
        <Route exact path="/">
          <Goods
            favGoods={favGoods}
            toggleFav={addToFavorite}
            showGoods={goods}
            onModal={handleModalOpen}
          />
        </Route>
        <Route path="/cart">
          <CartGoods
            allGoods={goods}
            delCartItem={deleteModalOpen}
            cartProducts={goodsCart}
          />
        </Route>
        <Route path="/favorite">
          <Favorite
            removeFav={removeFromFavorite}
            favGoods={favGoods}
            favProduct={favGoods}
            allGoods={goods}
          />
        </Route>
      </Switch>

      {isShowModal && <Modal modalBtns={cartActions} closeModal={handleModalClose}
        header={'Adding to cart...'}
        text={`Do you want to add "${goods[currentCartId].name.toUpperCase()}" in cart?`} />}
      {isShowModalDelete && <Modal modalBtns={deleteCartActions} closeModal={handleModalClose}
        header={'Deleting from cart...'}
        text={`Do you want to delete "${goods[currentCartId].name.toUpperCase()}" from cart?`}
      />}
    </>
  );
};

export default App;
