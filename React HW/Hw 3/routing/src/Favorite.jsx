import "./App.css";
import React, { useEffect, useState } from "react";
import Cards from "./cards";

const Favorite = ({ favProduct, favGoods, removeFav, allGoods }) => {
  const [isLoading, setisLoading] = useState(true);

  useEffect(() => {
    if (allGoods.length > 0) {
      setisLoading(false);
    }
  }, [allGoods]);

  const favProducts = [];

  if (!isLoading) {
    favProduct.forEach((id) => {
      const targetFav = allGoods.find((item) => item.id === id);
      favProducts.push(targetFav);
    });
  }

  return (
    <div className="cards">
      {favProduct.length === 0 || isLoading ? (
        <p>NO FAVORITES</p>
      ) : (
        favProducts.map((product) => (
          <Cards
            toggleFav={removeFav}
            favGoods={favGoods}
            product={product}
            key={product.id}
          />
        ))
      )}
    </div>
  );
};

export default Favorite;
