import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import star from "./star.svg";
import unStar from "./unstar.svg";

const Cards = ({ product, onModal, toggleFav, favGoods }) => {
  const [starState, setStarState] = useState(true);
  let { name, price, src, article, color, dataFav, closeBtn, id } = product;

  useEffect(() => {
    const isFaviriteCard = favGoods.find((id) => id === product.id);
    if (isFaviriteCard) {
      setStarState(false);
    }
  }, [starState]);

  if (closeBtn) {
    closeBtn = "X";
  }
  const handleFavorite = () => {
    setStarState(!starState);
    toggleFav(id);
  };

  return (
    <div className="card-container" data-x={dataFav}>
      <span className="star">
        <img
          className="starClick"
          onClick={handleFavorite}
          src={starState ? star : unStar}
        ></img>
      </span>
      <img src={src} alt='img'></img>
      <h5 className="card-title">{name}</h5>
      <span className="list-group-item">Article: {article}</span>
      <span className="list-group-item">Color: {color}</span>
      <div className="card-footer">
        <span>${price}</span>
        <button
          onClick={() => {
            onModal(id);
          }}
          className="btn-primary btn"
        >
          Add to cart
        </button>
      </div>
    </div>
  );
};

Cards.propTypes = {
  name: PropTypes.string,
  price: PropTypes.string,
  src: PropTypes.string,
  article: PropTypes.string,
  color: PropTypes.string,
  dataFav: PropTypes.number,
};

export default Cards;
