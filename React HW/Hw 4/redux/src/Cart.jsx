import "./App.css";
import React from "react";
import ProductInCart from "./Pages/ProductInCart";
import { useSelector } from "react-redux";

const CartGoods = () => {
  const cartContentId = useSelector((state) => state.cart);
  const totalGoods = useSelector((state) => state.general.products);
  const isLoadingCartItems = useSelector((state) => state.general.isLoading);

  const inCartProducts = [];

  cartContentId.forEach((id) => {
    const targetCart = totalGoods.find((item) => item.id === id);
    inCartProducts.push(targetCart);
  })
 
  return (
    <div className="cards">
      {inCartProducts.length === 0 || isLoadingCartItems ? (
        <p>Products is loading ...</p>
      ) : (
        inCartProducts.map((product) => (
          <ProductInCart
            product={product}
            key={product.id}
          />
        ))
      )}
    </div>
  );
};

export default CartGoods;
