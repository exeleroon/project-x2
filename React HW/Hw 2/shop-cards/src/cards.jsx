import React, {Component} from 'react'
import Modal from './Components/modal/Modal' 
import Button from './Components/buttons/BtnComponent'
import PropTypes from 'prop-types'
import star from './star.svg'
import unStar from './unstar.svg'


class Cards extends React.Component {    
  constructor(props) {
    super()
      this.state = {
        ok: true,
        svg: star,
        svg2: unStar,
        test: true,
        // article: this.props.article,
        isOpenFirstModal: false,
        star: false,
      }
  }

  componentDidMount() {
     for (let key in localStorage) {
       if (this.props.dataFav == key) {
        this.setState({star: !this.state.star})
        this.state.svg = this.state.svg2
        // console.log(this.state.test);
       }
}}
    
      openFirstModal = (e) => {
        this.setState({isOpenFirstModal: !this.state.isOpenFirstModal});
        this.favour(e);
      }
    
      closeModalWindow = (e) => {
        if (this.state.isOpenFirstModal && e.target.classList.contains('modal') || e.target.classList.contains('close')) {
          this.setState({isOpenFirstModal: !this.state.isOpenFirstModal})
        }
      }

      favour = (e) => {
        const targetEl = e.target;
        let closeEl = targetEl.closest('.card-container')
        const keyEl = closeEl.getAttribute('data-x')
        const propsName = this.props.name;
        const propsArticle = this.props.article;

        if (!this.state.star) {
          this.setState({star: !this.state.star})
          targetEl.setAttribute('src', unStar)
          localStorage.setItem(keyEl, !this.state.star)
          localStorage.setItem(propsName, propsArticle);
        } else {
          this.setState({star: !this.state.star});
          e.target.setAttribute('src' , star);
          localStorage.removeItem(keyEl, star);
          localStorage.removeItem(propsName, propsArticle);
        } 
}

    render() {
      let {name, price, src, article, color, dataFav} = this.props;
    
        return (
            <>
            {this.state.isOpenFirstModal && (
                <Modal closeBtn={true} closeModal={this.closeModalWindow}
                text={`You has added "${this.props.name}" to your shopping card`} 
                  actions={(
                  <>
                    <Button bgColor={'rgba(0, 0, 0, 0.219)'} className={'modalBtn close'} text={'OK'} />
                    </>
                  )}
                />
            )}
                <div className="card-container" data-x={dataFav} >
                    <span className='star' >
                        <img className="starClick" onClick={this.favour} src={this.state.svg} ></img>
                    </span>
                      <img src={src}></img>
                      <h5 className="card-title">{name}</h5>
                        <span className='list-group-item'>Article: {article}</span>
                        <span className='list-group-item'>Color: {color}</span>
                <div className="card-footer">
                    <span>${price}</span>
                    <button onClick={this.openFirstModal} className='btn-primary btn'>Add to cart</button>
                </div>
        </div>
        </>
        )
    }
}

Cards.propTypes = {
  name: PropTypes.string,
  price: PropTypes.string,
  src: PropTypes.string,
  article: PropTypes.string,
  color: PropTypes.string,
  dataFav: PropTypes.number
}



export default Cards