import './App.css';
import React, { Component } from 'react'
import Cards from './cards'
import { useHistory } from 'react-router-dom'

class App extends Component {
  constructor(props) {
    super(props)
    
    this.state = {
      goods: []
    }
  }

  componentDidMount() {
    fetch('http://localhost:3000/goods.json')
      .then(res => {
        return res.json();
      })
      .then((data) => {
       this.setState({goods: data.goods})
      })
  }


  render() {
    let cards = this.state.goods.map((obj, index) => {
      return <Cards key={index} dataFav={index} name={obj.name.charAt(0).toUpperCase() + obj.name.slice(1)} price={obj.price} src={obj.src} 
      article={obj.article} color={obj.color}/>      

    })
    return (
      <div className='cards'>
        {cards}
      </div>
      
    )
  }
}

export default App





