import React, {Component} from 'react'
import PropTypes from 'prop-types'

class Modal extends Component {
  render() {
    let {header, closeBtn, text, actions, closeModal} = this.props;
      if (closeBtn) {
        closeBtn = 'X'
      }

    return (
        <div onClick={closeModal} className="modal show">
          <div className="modal-dialog">
            <div className='modal-content'>
            <div className='modal-header'>{header}
                <span onClick={closeModal} className='close-Btn close'>
                  {closeBtn}
                </span>
            </div>
              <div className='modal-body'>{text}
              </div>
            <div className='modal-body btns'>
              {actions}
              </div>
            </div>
          </div>
      </div>
    )}
}

Modal.propTypes = {
  header: PropTypes.string,
  closeModal: PropTypes.func,
  text: PropTypes.string
}

Modal.defaultProps = {
  closeBtn: 'X'
}


export default Modal


