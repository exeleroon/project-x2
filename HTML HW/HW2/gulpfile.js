const gulp = require('gulp');
const concat = require('gulp-concat');
const del = require('delete');
const terser = require('gulp-terser');
const htmlmin = require('gulp-htmlmin');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass');
const imagemin = require('gulp-imagemin');


function buildCSS() {
    return gulp.src('src/scss/index.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('main.css'))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(gulp.dest('dist'))
}

function buildHTML() {
    return gulp.src('*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('dist'))
}

function buildJS() {
    return gulp.src('src/js/*.js')
        .pipe(terser())
        .pipe(gulp.dest('dist'))
}

function clean() {
    return del('dist/**', { force: true });
}

function buildImg() {
    return gulp.src('src/img/*')
            .pipe(imagemin())
            .pipe(gulp.dest('dist/img'))
}

function build() {
    return gulp.series([
        clean,
        gulp.parallel([
            buildCSS,
            buildHTML,
            buildJS,
            buildImg
        ])
    ])
}

function start() {
    gulp.watch('src/**/*', build())
}

exports.buildHTML = buildHTML;
exports.buildCSS = buildCSS;
exports.buildJS = buildJS;
exports.clean = clean;
exports.start = start;
exports.default = build();