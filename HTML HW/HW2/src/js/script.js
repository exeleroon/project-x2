const burger = document.querySelector('.menu-btn');
const headerCont = document.querySelector('.header-container');

burger.addEventListener('click', function (event) {
    if (event.target == burger) {
        burger.classList.toggle('open')
        headerCont.classList.toggle('active')
    }
})
